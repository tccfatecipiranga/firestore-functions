export interface Address {
    zipCode: string;
    street: string;
    number: number;
    complement?: string;
    district: string;
    city: string;
    state: string;
    createdDate: any;
  }
