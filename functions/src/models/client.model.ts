import { Address } from './address.model';

export interface Client {
  id?: string;
  name: string;
  type: string;
  document: number;
  address: Address;
  email: string;
  cellphone: string;
  phone: string;
  createdDate?: string;
  alteredDate?: string;
}
