// classe para criar usuario e que vai receber o observable e etc
export interface User {
  userId?: string;
  name?: string;
  email: string;
  typePerson?: string;
  doc?: string;
  currentWorkOrder?: number;
}
