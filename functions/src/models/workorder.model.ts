import { Product } from "./product.model";
import { Service } from "./service.model";
import { ProductType } from "./productType.model";
import { Brand } from "./brand.model";
import { Client } from "./client.model";

export interface WorkOrder {
  id?: string;
  number?: number;
  status?: string;
  client: string;
  typeProduct: string;
  brand: string;
  model?: string;
  serial?: string;
  problem?: string;
  solution?: string;
  approved?: boolean;
  descProblem?: string;
  products?: string[];
  services?: string[];
  taxExtra?: number;
  totalValue?: number;
  createdDate?: any;
  sendEmail?: boolean; //TODO: Criar uma verificação para enviar o email ou não.
}

export interface WorkOrderDoc {
  id?: string;
  number?: number;
  status?: string;
  client?: Client;
  typeProduct?: ProductType;
  brand?: Brand;
  model?: string;
  serial?: string;
  problem?: string;
  solution?: string;
  approved?: boolean;
  descProblem?: string;
  products?: Product[];
  services?: Service[];
  taxExtra?: number;
  totalValue?: number;
  createdDate?: any;
}
