"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");
admin.initializeApp();
// const SENDGRID_API_KEY = functions.config().sendgrid;
// sgMail.setApiKey(SENDGRID_API_KEY.key);
const emailUser = 'tccosbr@gmail.com';
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: emailUser,
        pass: 'tcclgdell'
    }
});
const db = admin.firestore();
// Triggers Firestore
exports.updateWorkOrder = functions.firestore // Send an email to client when updating work order
    .document('users/{userId}/workorders/{workOrderId}')
    .onUpdate((change, context) => {
    const userId = context.params.userId;
    const workOrderId = context.params.workOrderId;
    console.log('START | workOrder: ' + workOrderId);
    const workOrder = change.after.data();
    const userRef = getUserCompanyRef(userId);
    const clientRef = getClientRef(userId, workOrder.client);
    const brandRef = getBrandRef(userId, workOrder.brand);
    const productTypeRef = getProductTypeRef(userId, workOrder.typeProduct);
    return db
        .getAll(clientRef, brandRef, productTypeRef, userRef)
        .then(docs => {
        const client = docs[0].data();
        const brand = docs[1].data();
        const productType = docs[2].data();
        const user = docs[3].data();
        const type = 'Atualizada OS: ' + workOrder.number;
        // console.log(workorder);
        // console.log('Client Document: ' + client.name);
        // console.log(client);
        // console.log('Brand Document: ' + brand.name);
        // console.log(brand);
        // console.log('Product Type Document: ' + productType.name);
        // console.log(productType);
        const msg = getMessageWorkOrder(type, workOrder, client, brand, productType, user);
        const mailOptions = {
            from: emailUser,
            to: client.email,
            subject: type,
            html: msg // email content in HTML
        };
        return transporter.sendMail(mailOptions);
    })
        .then(() => {
        console.log('email enviado');
    })
        .catch(err => {
        console.log(err);
    });
});
exports.createWorkOrder = functions.firestore
    .document('users/{userId}/workorders/{workOrderId}')
    .onCreate((change, context) => {
    const userId = context.params.userId;
    const workOrderId = context.params.workOrderId;
    console.log('START | workOrder: ' + workOrderId);
    const workOrder = change.data();
    const clientRef = getClientRef(userId, workOrder.client);
    const userRef = getUserCompanyRef(userId);
    const brandRef = getBrandRef(userId, workOrder.brand);
    const productTypeRef = getProductTypeRef(userId, workOrder.typeProduct);
    return db
        .getAll(clientRef, brandRef, productTypeRef, userRef)
        .then(docs => {
        const client = docs[0].data();
        const brand = docs[1].data();
        const productType = docs[2].data();
        const user = docs[3].data();
        const type = 'Criada OS: ' + workOrder.number;
        // console.log(workorder);
        // console.log('Client Document: ' + client.name);
        // console.log(client);
        // console.log('Brand Document: ' + brand.name);
        // console.log(brand);
        // console.log('Product Type Document: ' + productType.name);
        // console.log(productType);
        const msg = getMessageWorkOrder(type, workOrder, client, brand, productType, user);
        const mailOptions = {
            from: emailUser,
            to: client.email,
            subject: type,
            html: msg // email content in HTML
        };
        return transporter.sendMail(mailOptions);
    })
        .then(() => {
        console.log('email enviado');
    })
        .catch(err => {
        console.log(err);
    });
});
exports.updateClient = functions.firestore // Send an email to client when updating work order
    .document('users/{userId}/clients/{clientId}')
    .onUpdate((change, context) => {
    const userId = context.params.userId;
    const clientId = context.params.clientId;
    console.log('START | Client: ' + clientId);
    const client = change.after.data();
    return getUserCompanyRef(userId)
        .get()
        .then(doc => {
        const user = doc.data();
        const type = 'Atualizado cadastro';
        const msg = getMessageClient(type, client, user);
        const mailOptions = {
            from: emailUser,
            to: client.email,
            subject: type,
            html: msg // email content in HTML
        };
        return transporter.sendMail(mailOptions);
    })
        .then(() => {
        console.log('email enviado');
    })
        .catch(err => {
        console.log(err);
    });
});
exports.createClient = functions.firestore
    .document('users/{userId}/clients/{clientId}')
    .onCreate((change, context) => {
    const userId = context.params.userId;
    const clientId = context.params.clientId;
    console.log('START | Client: ' + clientId);
    const client = change.data();
    return getUserCompanyRef(userId)
        .get()
        .then(doc => {
        const user = doc.data();
        const type = 'Criado cadastro';
        const msg = getMessageClient(type, client, user);
        const mailOptions = {
            from: emailUser,
            to: client.email,
            subject: type,
            html: msg // email content in HTML
        };
        return transporter.sendMail(mailOptions);
    })
        .then(() => {
        console.log('email enviado');
    })
        .catch(err => {
        console.log(err);
    });
});
function getUserCompanyRef(userId) {
    return db.doc('users/' + userId);
}
function getBrandRef(userId, docId) {
    return db.doc('users/' + userId + '/brands/' + docId);
}
function getClientRef(userId, docId) {
    return db.doc('users/' + userId + '/clients/' + docId);
}
function getProductTypeRef(userId, docId) {
    return db.doc('users/' + userId + '/productTypes/' + docId);
}
function getMessageClient(type, client, user) {
    return `
  <head>
  <title> </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  
  <style type="text/css">
@media only screen and (min-width:480px) {
  .mj-column-per-100 {
    width: 100% !important;
    max-width: 100%;
  }

  .mj-column-per-50 {
    width: 50% !important;
    max-width: 50%;
  }
}
</style>
  <style type="text/css">
@media only screen and (max-width:480px) {
  table.full-width-mobile {
    width: 100% !important;
  }

  td.full-width-mobile {
    width: auto !important;
  }
}
</style>
</head>
<body>
  <div style="">
  <div style="Margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
      <tbody>
        <tr>
          <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;" align="center" valign="top">
            <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                <tr>
                  <td align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0px;">
                      <tbody>
                        <tr>
                          <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100px;" width="100"> <img height="auto" src="https://osbr-fatec-ipi.firebaseapp.com/assets/images/navbarlogo.jpg" style="line-height: 100%; -ms-interpolation-mode: bicubic; border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%;" width="100"> </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                    <p style="display: block; border-top: solid 4px #325c80; font-size: 1; margin: 0px auto; width: 100%;"> </p>
                  </td>
                </tr>
                <tr>
                  <td vertical-align="top" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 20px 0; word-break: break-word;">
                    <div style="background:white;background-color:white;Margin:0px auto;max-width:600px;">
                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: white; background-color: white; width: 100%;" width="100%" bgcolor="white">
                        <tbody>
                          <tr>
                            <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;" align="center" valign="top">
                              <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                                <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Helvetica Neue;font-size:20px;font-style:italic;line-height:1;text-align:left;color:#626262;"> ${type} </div>
                                    </td>
                                  </tr>
                                  </tr>${client.type === 'fisica'
        ? `
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Nome: ${client.name} </div>
                                    </td>
                                  </tr> 
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> CPF: ${client.document} </div>
                                    </td>
                                  </tr> `
        : `
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Razão social: ${client.name} </div>
                                        </td>
                                      </tr> 
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> CNPJ: ${client.document} </div>
                                        </td>
                                      </tr> `}
                                  
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> CEP: ${client.address.zipCode} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <tr>
                                      <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Estado: ${client.address.state} </div>
                                      </td>
                                    </tr>
                                    <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Cidade: ${client.address.city} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Endereço: ${client.address.street} </div>
                                    </td>
                                  </tr>
                                <tr>
                                  <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Celular: ${client.cellphone} </div>
                                  </td>
                                </tr>
                            ${client.phone !== undefined
        ? `
                              <tr>
                                <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                  <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Telefone: ${client.phone} </div>
                                </td>
                              </tr>`
        : ``}
                                </table>
                              </div>
                              <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Helvetica Neue;font-size:20px;font-style:italic;line-height:1;text-align:left;color:#626262;"> Prestador de serviço: </div>
                                    </td>
                                  </tr>

                                </tr>${user.typePerson === 'fisica'
        ? `
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Nome: ${user.name} </div>
                                    </td>
                                  </tr> `
        : `
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Razão social: ${user.name} </div>
                                        </td>
                                      </tr> 
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> CNPJ: ${user.doc} </div>
                                        </td>
                                      </tr> `}
                                </table>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</body>`;
}
function getMessageWorkOrder(type, workOrder, client, brand, productType, user) {
    return `
  <head>
  <title> </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  
  <style type="text/css">
@media only screen and (min-width:480px) {
  .mj-column-per-100 {
    width: 100% !important;
    max-width: 100%;
  }

  .mj-column-per-50 {
    width: 50% !important;
    max-width: 50%;
  }
}
</style>
  <style type="text/css">
@media only screen and (max-width:480px) {
  table.full-width-mobile {
    width: 100% !important;
  }

  td.full-width-mobile {
    width: auto !important;
  }
}
</style>
</head>
<body>
  <div style="">
  <div style="Margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
      <tbody>
        <tr>
          <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;" align="center" valign="top">
            <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                <tr>
                  <td align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse; border-spacing: 0px;">
                      <tbody>
                        <tr>
                          <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100px;" width="100"> <img height="auto" src="https://osbr-fatec-ipi.firebaseapp.com/assets/images/navbarlogo.jpg" style="line-height: 100%; -ms-interpolation-mode: bicubic; border: 0; display: block; outline: none; text-decoration: none; height: auto; width: 100%;" width="100"> </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                    <p style="display: block; border-top: solid 4px #325c80; font-size: 1; margin: 0px auto; width: 100%;"> </p>
                  </td>
                </tr>
                <tr>
                  <td vertical-align="top" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 20px 0; word-break: break-word;">
                    <div style="background:white;background-color:white;Margin:0px auto;max-width:600px;">
                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: white; background-color: white; width: 100%;" width="100%" bgcolor="white">
                        <tbody>
                          <tr>
                            <td style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;" align="center" valign="top">
                              <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                                
                                <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Helvetica Neue;font-size:20px;font-style:italic;line-height:1;text-align:left;color:#626262;"> ${type} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Estado: ${workOrder.status} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Marca: ${brand.name} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Tipo do produto: ${productType.name} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Modelo: ${workOrder.model} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Serial: ${workOrder.serial} </div>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Problema: ${workOrder.problem} </div>
                                    </td>
                                  </tr>${workOrder.solution !== undefined ||
        workOrder.solution !== ''
        ? `
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Solução: ${workOrder.solution} </div>
                                    </td>
                                  </tr> `
        : ''}
                                  ${workOrder.totalValue !== undefined
        ? `
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Preço: R$ ${workOrder.totalValue} </div>
                                    </td>
                                  </tr>
                                  `
        : ''}
                                </table>
                              </div>
                              <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                                  
                              </div>
                              <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; vertical-align: top;" width="100%" valign="top">
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Helvetica Neue;font-size:20px;font-style:italic;line-height:1;text-align:left;color:#626262;"> Prestador de serviço: </div>
                                    </td>
                                  </tr>

                                </tr>${user.typePerson === 'fisica'
        ? `
                                  <tr>
                                    <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Nome: ${user.name} </div>
                                    </td>
                                  </tr> `
        : `
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> Razão social: ${user.name} </div>
                                        </td>
                                      </tr> 
                                      <tr>
                                        <td align="left" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-size: 0px; padding: 10px 25px; word-break: break-word;">
                                          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#525252;"> CNPJ: ${user.doc} </div>
                                        </td>
                                      </tr> `}
                                </table>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</body>
  `;
}
//# sourceMappingURL=index.js.map